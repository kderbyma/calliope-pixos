/*                                                 *\
** ----------------------------------------------- **
**             Calliope - Site Generator   	       **
** ----------------------------------------------- **
**  Copyright (c) 2020-2021 - Kyle Derby MacInnis  **
**                                                 **
**    Any unauthorized distribution or transfer    **
**       of this work is strictly prohibited.      **
**                                                 **
**               All Rights Reserved.              **
** ----------------------------------------------- **
\*                                                 */

import Resources from "../../engine/utils/resources.jsx";
// Tileset Schema
export default {
  src: Resources.artResourceUrl("sewer.gif"),
  sheetSize: [256, 256],
  tileSize: 16,
  bgColor: [31, 20, 29],
  // Tile Locations on resource (based on size)
  tiles: {
    0: [1, 1],
    1: [2, 2],
    2: [2, 1],
    3: [2, 0],
    4: [1, 0],
    5: [0, 0],
    6: [0, 1],
    7: [0, 2],
    8: [1, 2],
    9: [4, 1],
    10: [4, 0],
    11: [3, 0],
    12: [3, 1],
    13: [3, 2],
    14: [0.5, 7.5],
    15: [4, 2],
    16: [3, 3],
    20: [6, 1],
    21: [7, 2],
    22: [7, 1],
    23: [7, 0],
    24: [6, 0],
    25: [5, 0],
    26: [5, 1],
    27: [5, 2],
    28: [6, 2],
    29: [9, 1],
    30: [9, 0],
    31: [8, 0],
    32: [8, 1],
    50: [5, 3],
    100: [0, 3],
    101: [0, 5],
    102: [0, 6],
  },
  // Geometries for the tileset
  // type --> walkability -- 1/0 --> [down,left,up,right]
  tileGeometry: {
    // All
    0: {
      vertices: [
        [
          [0, 1, 0],
          [1, 1, 0],
          [1, 0, 0],
        ],
        [
          [0, 1, 0],
          [1, 0, 0],
          [0, 0, 0],
        ],
      ],
      tiles: [
        [
          [0, 1],
          [1, 1],
          [1, 0],
        ],
        [
          [0, 1],
          [1, 0],
          [0, 0],
        ],
      ],
      type: parseInt("1111",2),
    },
    // None
    1: {
      vertices: [
        [
          [0, 1, 0],
          [1, 1, 0],
          [1, 0, 0],
        ],
        [
          [0, 1, 0],
          [1, 0, 0],
          [0, 0, 0],
        ],
      ],
      tiles: [
        [
          [0, 1],
          [1, 1],
          [1, 0],
        ],
        [
          [0, 1],
          [1, 0],
          [0, 0],
        ],
      ],
      type: parseInt("0000",2),
    },
    
    50: {
      vertices: [
        [
          [0.25, 0, 0.25],
          [0.25, 1, 0.25],
          [0.5, 1, 0.25],
        ],
        [
          [0.25, 0, 0.25],
          [0.5, 1, 0.25],
          [0.5, 0, 0.25],
        ],
        [
          [0.5, 0, 0.5],
          [0.5, 1, 0.5],
          [0.75, 1, 0.5],
        ],
        [
          [0.5, 0, 0.5],
          [0.75, 1, 0.5],
          [0.75, 0, 0.5],
        ],
        [
          [0.75, 0, 0.75],
          [0.75, 1, 0.75],
          [1, 1, 0.75],
        ],
        [
          [0.75, 0, 0.75],
          [1, 1, 0.75],
          [1, 0, 0.75],
        ],
        [
          [0.25, 0, 0],
          [0.25, 1, 0],
          [0.25, 1, 0.25],
        ],
        [
          [0.25, 0, 0],
          [0.25, 1, 0.25],
          [0.25, 0, 0.25],
        ],
        [
          [0.5, 0, 0.25],
          [0.5, 1, 0.25],
          [0.5, 1, 0.5],
        ],
        [
          [0.5, 0, 0.25],
          [0.5, 1, 0.5],
          [0.5, 0, 0.5],
        ],
        [
          [0.75, 0, 0.5],
          [0.75, 1, 0.5],
          [0.75, 1, 0.75],
        ],
        [
          [0.75, 0, 0.5],
          [0.75, 1, 0.75],
          [0.75, 0, 0.75],
        ],
        [
          [1, 0, 0.75],
          [1, 1, 0.75],
          [1, 1, 1],
        ],
        [
          [1, 0, 0.75],
          [1, 1, 1],
          [1, 0, 1],
        ],
        [
          [0, 0, 0],
          [1, 0, 0],
          [1, 0, 1],
        ],
        [
          [0, 0, 0],
          [1, 0, 1],
          [0, 0, 1],
        ],
        [
          [0, 1, 0],
          [1, 1, 0],
          [1, 1, 1],
        ],
        [
          [0, 1, 0],
          [1, 1, 1],
          [0, 1, 1],
        ],
      ],
      tiles: [
        [
          [0, 0.75],
          [1, 0.75],
          [1, 0.5],
        ],
        [
          [0, 0.75],
          [1, 0.5],
          [0, 0.5],
        ],
        [
          [0, 0.5],
          [1, 0.5],
          [1, 0.25],
        ],
        [
          [0, 0.5],
          [1, 0.25],
          [0, 0.25],
        ],
        [
          [0, 0.25],
          [1, 0.25],
          [1, 0],
        ],
        [
          [0, 0.25],
          [1, 0.0],
          [0, 0],
        ],
        [
          [0, 2],
          [1, 2],
          [1, 1.75],
        ],
        [
          [0, 2],
          [1, 1.75],
          [0, 1.75],
        ],
        [
          [0, 1.75],
          [1, 1.75],
          [1, 1.5],
        ],
        [
          [0, 1.75],
          [1, 1.5],
          [0, 1.5],
        ],
        [
          [0, 1.5],
          [1, 1.5],
          [1, 1.25],
        ],
        [
          [0, 1.5],
          [1, 1.25],
          [0, 1.25],
        ],
        [
          [0, 1.25],
          [1, 1.25],
          [1, 1],
        ],
        [
          [0, 1.25],
          [1, 1],
          [0, 1],
        ],
        [
          [0, 3],
          [1, 3],
          [1, 2],
        ],
        [
          [0, 3],
          [1, 2],
          [0, 2],
        ],
        [
          [0, 3],
          [1, 3],
          [1, 2],
        ],
        [
          [0, 3],
          [1, 2],
          [0, 2],
        ],
      ],
      w: [
        [
          [0, 1, 0],
          [1, 1, 1],
          [1, 0, 1],
        ],
        [
          [0, 1, 0],
          [1, 0, 1],
          [0, 0, 0],
        ],
      ],
      type: parseInt("0101",2),
    },
    51: {
      vertices: [
        [
          [0, 0.75, 0.25],
          [1, 0.75, 0.25],
          [1, 0.5, 0.25],
        ],
        [
          [0, 0.75, 0.25],
          [1, 0.5, 0.25],
          [0, 0.5, 0.25],
        ],
        [
          [0, 0.5, 0.5],
          [1, 0.5, 0.5],
          [1, 0.25, 0.5],
        ],
        [
          [0, 0.5, 0.5],
          [1, 0.25, 0.5],
          [0, 0.25, 0.5],
        ],
        [
          [0, 0.25, 0.75],
          [1, 0.25, 0.75],
          [1, 0, 0.75],
        ],
        [
          [0, 0.25, 0.75],
          [1, 0, 0.75],
          [0, 0, 0.75],
        ],
        [
          [0, 0.75, 0],
          [1, 0.75, 0],
          [1, 0.75, 0.25],
        ],
        [
          [0, 0.75, 0],
          [1, 0.75, 0.25],
          [0, 0.75, 0.25],
        ],
        [
          [0, 0.5, 0.25],
          [1, 0.5, 0.25],
          [1, 0.5, 0.5],
        ],
        [
          [0, 0.5, 0.25],
          [1, 0.5, 0.5],
          [0, 0.5, 0.5],
        ],
        [
          [0, 0.25, 0.5],
          [1, 0.25, 0.5],
          [1, 0.25, 0.75],
        ],
        [
          [0, 0.25, 0.5],
          [1, 0.25, 0.75],
          [0, 0.25, 0.75],
        ],
        [
          [0, 0, 0.75],
          [1, 0, 0.75],
          [1, 0, 1],
        ],
        [
          [0, 0, 0.75],
          [1, 0, 1],
          [0, 0, 1],
        ],
        [
          [0, 1, 0],
          [0, 0, 0],
          [0, 0, 1],
        ],
        [
          [0, 1, 0],
          [0, 0, 1],
          [0, 1, 1],
        ],
        [
          [1, 1, 0],
          [1, 0, 0],
          [1, 0, 1],
        ],
        [
          [1, 1, 0],
          [1, 0, 1],
          [1, 1, 1],
        ],
      ],
      tiles: [
        [
          [0, 0.75],
          [1, 0.75],
          [1, 0.5],
        ],
        [
          [0, 0.75],
          [1, 0.5],
          [0, 0.5],
        ],
        [
          [0, 0.5],
          [1, 0.5],
          [1, 0.25],
        ],
        [
          [0, 0.5],
          [1, 0.25],
          [0, 0.25],
        ],
        [
          [0, 0.25],
          [1, 0.25],
          [1, 0],
        ],
        [
          [0, 0.25],
          [1, 0.0],
          [0, 0],
        ],
        [
          [0, 2],
          [1, 2],
          [1, 1.75],
        ],
        [
          [0, 2],
          [1, 1.75],
          [0, 1.75],
        ],
        [
          [0, 1.75],
          [1, 1.75],
          [1, 1.5],
        ],
        [
          [0, 1.75],
          [1, 1.5],
          [0, 1.5],
        ],
        [
          [0, 1.5],
          [1, 1.5],
          [1, 1.25],
        ],
        [
          [0, 1.5],
          [1, 1.25],
          [0, 1.25],
        ],
        [
          [0, 1.25],
          [1, 1.25],
          [1, 1],
        ],
        [
          [0, 1.25],
          [1, 1],
          [0, 1],
        ],
        [
          [0, 3],
          [1, 3],
          [1, 2],
        ],
        [
          [0, 3],
          [1, 2],
          [0, 2],
        ],
        [
          [0, 3],
          [1, 3],
          [1, 2],
        ],
        [
          [0, 3],
          [1, 2],
          [0, 2],
        ],
      ],
      w: [
        [
          [0, 1, 0],
          [1, 1, 0],
          [1, 0, 1],
        ],
        [
          [0, 1, 0],
          [1, 0, 1],
          [0, 0, 1],
        ],
      ],
      type: parseInt("1010",2),
    },
    52: {
      vertices: [
        [
          [0.75, 1, 0.25],
          [0.75, 0, 0.25],
          [0.5, 0, 0.25],
        ],
        [
          [0.75, 1, 0.25],
          [0.5, 0, 0.25],
          [0.5, 1, 0.25],
        ],
        [
          [0.5, 1, 0.5],
          [0.5, 0, 0.5],
          [0.25, 0, 0.5],
        ],
        [
          [0.5, 1, 0.5],
          [0.25, 0, 0.5],
          [0.25, 1, 0.5],
        ],
        [
          [0.25, 1, 0.75],
          [0.25, 0, 0.75],
          [0, 0, 0.75],
        ],
        [
          [0.25, 1, 0.75],
          [0, 0, 0.75],
          [0, 1, 0.75],
        ],
        [
          [0.75, 1, 0],
          [0.75, 0, 0],
          [0.75, 0, 0.25],
        ],
        [
          [0.75, 1, 0],
          [0.75, 0, 0.25],
          [0.75, 1, 0.25],
        ],
        [
          [0.5, 1, 0.25],
          [0.5, 0, 0.25],
          [0.5, 0, 0.5],
        ],
        [
          [0.5, 1, 0.25],
          [0.5, 0, 0.5],
          [0.5, 1, 0.5],
        ],
        [
          [0.25, 1, 0.5],
          [0.25, 0, 0.5],
          [0.25, 0, 0.75],
        ],
        [
          [0.25, 1, 0.5],
          [0.25, 0, 0.75],
          [0.25, 1, 0.75],
        ],
        [
          [0, 1, 0.75],
          [0, 0, 0.75],
          [0, 0, 1],
        ],
        [
          [0, 1, 0.75],
          [0, 0, 1],
          [0, 1, 1],
        ],
        [
          [1, 1, 0],
          [0, 1, 0],
          [0, 1, 1],
        ],
        [
          [1, 1, 0],
          [0, 1, 1],
          [1, 1, 1],
        ],
        [
          [1, 0, 0],
          [0, 0, 0],
          [0, 0, 1],
        ],
        [
          [1, 0, 0],
          [0, 0, 1],
          [1, 0, 1],
        ],
      ],
      tiles: [
        [
          [0, 0.75],
          [1, 0.75],
          [1, 0.5],
        ],
        [
          [0, 0.75],
          [1, 0.5],
          [0, 0.5],
        ],
        [
          [0, 0.5],
          [1, 0.5],
          [1, 0.25],
        ],
        [
          [0, 0.5],
          [1, 0.25],
          [0, 0.25],
        ],
        [
          [0, 0.25],
          [1, 0.25],
          [1, 0],
        ],
        [
          [0, 0.25],
          [1, 0.0],
          [0, 0],
        ],
        [
          [0, 2],
          [1, 2],
          [1, 1.75],
        ],
        [
          [0, 2],
          [1, 1.75],
          [0, 1.75],
        ],
        [
          [0, 1.75],
          [1, 1.75],
          [1, 1.5],
        ],
        [
          [0, 1.75],
          [1, 1.5],
          [0, 1.5],
        ],
        [
          [0, 1.5],
          [1, 1.5],
          [1, 1.25],
        ],
        [
          [0, 1.5],
          [1, 1.25],
          [0, 1.25],
        ],
        [
          [0, 1.25],
          [1, 1.25],
          [1, 1],
        ],
        [
          [0, 1.25],
          [1, 1],
          [0, 1],
        ],
        [
          [0, 3],
          [1, 3],
          [1, 2],
        ],
        [
          [0, 3],
          [1, 2],
          [0, 2],
        ],
        [
          [0, 3],
          [1, 3],
          [1, 2],
        ],
        [
          [0, 3],
          [1, 2],
          [0, 2],
        ],
      ],
      w: [
        [
          [0, 1, 1],
          [1, 1, 0],
          [1, 0, 0],
        ],
        [
          [0, 1, 1],
          [1, 0, 0],
          [0, 0, 1],
        ],
      ],
      type: parseInt("0101",2),
    },
    53: {
      vertices: [
        [
          [1, 0.25, 0.25],
          [0, 0.25, 0.25],
          [0, 0.5, 0.25],
        ],
        [
          [1, 0.25, 0.25],
          [0, 0.5, 0.25],
          [1, 0.5, 0.25],
        ],
        [
          [1, 0.5, 0.5],
          [0, 0.5, 0.5],
          [0, 0.75, 0.5],
        ],
        [
          [1, 0.5, 0.5],
          [0, 0.75, 0.5],
          [1, 0.75, 0.5],
        ],
        [
          [1, 0.75, 0.75],
          [0, 0.75, 0.75],
          [0, 1, 0.75],
        ],
        [
          [1, 0.75, 0.75],
          [0, 1, 0.75],
          [1, 1, 0.75],
        ],
        [
          [1, 0.25, 0],
          [0, 0.25, 0],
          [0, 0.25, 0.25],
        ],
        [
          [1, 0.25, 0],
          [0, 0.25, 0.25],
          [1, 0.25, 0.25],
        ],
        [
          [1, 0.5, 0.25],
          [0, 0.5, 0.25],
          [0, 0.5, 0.5],
        ],
        [
          [1, 0.5, 0.25],
          [0, 0.5, 0.5],
          [1, 0.5, 0.5],
        ],
        [
          [1, 0.75, 0.5],
          [0, 0.75, 0.5],
          [0, 0.75, 0.75],
        ],
        [
          [1, 0.75, 0.5],
          [0, 0.75, 0.75],
          [1, 0.75, 0.75],
        ],
        [
          [1, 1, 0.75],
          [0, 1, 0.75],
          [0, 1, 1],
        ],
        [
          [1, 1, 0.75],
          [0, 1, 1],
          [1, 1, 1],
        ],
        [
          [1, 0, 0],
          [1, 1, 0],
          [1, 1, 1],
        ],
        [
          [1, 0, 0],
          [1, 1, 1],
          [1, 0, 1],
        ],
        [
          [0, 0, 0],
          [0, 1, 0],
          [0, 1, 1],
        ],
        [
          [0, 0, 0],
          [0, 1, 1],
          [0, 0, 1],
        ],
      ],
      tiles: [
        [
          [0, 0.75],
          [1, 0.75],
          [1, 0.5],
        ],
        [
          [0, 0.75],
          [1, 0.5],
          [0, 0.5],
        ],
        [
          [0, 0.5],
          [1, 0.5],
          [1, 0.25],
        ],
        [
          [0, 0.5],
          [1, 0.25],
          [0, 0.25],
        ],
        [
          [0, 0.25],
          [1, 0.25],
          [1, 0],
        ],
        [
          [0, 0.25],
          [1, 0.0],
          [0, 0],
        ],
        [
          [0, 2],
          [1, 2],
          [1, 1.75],
        ],
        [
          [0, 2],
          [1, 1.75],
          [0, 1.75],
        ],
        [
          [0, 1.75],
          [1, 1.75],
          [1, 1.5],
        ],
        [
          [0, 1.75],
          [1, 1.5],
          [0, 1.5],
        ],
        [
          [0, 1.5],
          [1, 1.5],
          [1, 1.25],
        ],
        [
          [0, 1.5],
          [1, 1.25],
          [0, 1.25],
        ],
        [
          [0, 1.25],
          [1, 1.25],
          [1, 1],
        ],
        [
          [0, 1.25],
          [1, 1],
          [0, 1],
        ],
        [
          [0, 3],
          [1, 3],
          [1, 2],
        ],
        [
          [0, 3],
          [1, 2],
          [0, 2],
        ],
        [
          [0, 3],
          [1, 3],
          [1, 2],
        ],
        [
          [0, 3],
          [1, 2],
          [0, 2],
        ],
      ],
      w: [
        [
          [0, 1, 1],
          [1, 1, 1],
          [1, 0, 0],
        ],
        [
          [0, 1, 1],
          [1, 0, 0],
          [0, 0, 0],
        ],
      ],
      type: parseInt("1010",2),
    },
    100: {
      vertices: [
        [
          [0, 0, -2],
          [0, 1, -2],
          [0, 1, 0],
        ],
        [
          [0, 0, -2],
          [0, 1, 0],
          [0, 0, 0],
        ],
      ],
      tiles: [
        [
          [0, 2],
          [1, 2],
          [1, 0],
        ],
        [
          [0, 2],
          [1, 0],
          [0, 0],
        ],
      ],
      type: parseInt("1011",2),
    },
    102: {
      vertices: [
        [
          [0, 1, -2],
          [1, 1, -2],
          [1, 1, 0],
        ],
        [
          [0, 1, -2],
          [1, 1, 0],
          [0, 1, 0],
        ],
      ],
      tiles: [
        [
          [0, 2],
          [1, 2],
          [1, 0],
        ],
        [
          [0, 2],
          [1, 0],
          [0, 0],
        ],
      ],
      type: parseInt("0111",2),
    },
    104: {
      vertices: [
        [
          [1, 1, -2],
          [1, 0, -2],
          [1, 0, 0],
        ],
        [
          [1, 1, -2],
          [1, 0, 0],
          [1, 1, 0],
        ],
      ],
      tiles: [
        [
          [0, 2],
          [1, 2],
          [1, 0],
        ],
        [
          [0, 2],
          [1, 0],
          [0, 0],
        ],
      ],
      type: parseInt("1110",2),
    },
    106: {
      vertices: [
        [
          [1, 0, -2],
          [0, 0, -2],
          [0, 0, 0],
        ],
        [
          [1, 0, -2],
          [0, 0, 0],
          [1, 0, 0],
        ],
      ],
      tiles: [
        [
          [0, 2],
          [1, 2],
          [1, 0],
        ],
        [
          [0, 2],
          [1, 0],
          [0, 0],
        ],
      ],
      type: parseInt("1101",2),
    },
  },
};

