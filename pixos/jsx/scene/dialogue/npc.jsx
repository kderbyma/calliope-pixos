export default {
    greeting : (name)=>`Hello ${name}, how are you doing?`,
    goodbye : (name) => `Bye ${name}, it was nice chatting with you.`,
    routine : (routine) => `I am just out running an errand. I need to ${routine}.`
}