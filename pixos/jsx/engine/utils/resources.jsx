/*                                                 *\
** ----------------------------------------------- **
**             Calliope - Site Generator   	       **
** ----------------------------------------------- **
**  Copyright (c) 2020-2021 - Kyle Derby MacInnis  **
**                                                 **
**    Any unauthorized distribution or transfer    **
**       of this work is strictly prohibited.      **
**                                                 **
**               All Rights Reserved.              **
** ----------------------------------------------- **
\*                                                 */

export default {
  tilesetRequestUrl: (id) => "/pixos/tilesets/" + id + ".tileset.jsx",
  zoneRequestUrl: (id) => "/pixos/maps/" + id + ".map.jsx",
  artResourceUrl: (art) => "/pixos/art/" + art,
};
